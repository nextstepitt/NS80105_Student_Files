package com.tc3.tc3service.controllers;

import java.util.ArrayList;
import java.util.Collection;

import com.tc3.tc3service.dto.SalesOrderDto;
import com.tc3.tc3service.models.SalesOrder;
import com.tc3.tc3service.services.SalesOrderHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/salesorders")
@CrossOrigin(origins = "http://localhost")
public class SalesOrderController {

    // All of the methods in this controller expect or return instances of Data Transfer Objects.
    // This is usually the best practice. Using DTOs results in more work, because of the conversion
    // from model object to DTO and back again (see the constructors in both classes). But, the
    // benefit is high: decoupling the client from the model in the service, and being able to
    // have multiple DTOs that represent interface to one model class, providing the client with
    // different versions of what they can see. For example, one DTO could be a "shorter" version
    // of a model object where all the information is not necessary to pass (and expose!).

    private SalesOrderHandler service;

    @Autowired
    public SalesOrderController(SalesOrderHandler service) {

        this.service = service;
    }

    @DeleteMapping("/{id}")
    void delete(@PathVariable("id") long id) {

        SalesOrderDto salesOrderDto = get(id);

        try {

            service.delete(new SalesOrder(salesOrderDto));
        }

        catch (Exception e) {

            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping
    Collection<SalesOrderDto> get() {

        try {

            Collection<SalesOrder> accounts = service.read();
            Collection<SalesOrderDto> salesOrderDtos = new ArrayList<SalesOrderDto>();

            accounts.forEach(account -> salesOrderDtos.add(new SalesOrderDto(account)));

            return salesOrderDtos;
        }

        catch (Exception e) {

            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    SalesOrderDto get(@PathVariable("id") long id) {

        try {

            SalesOrder account = service.read(id);

            if (account == null) {

                throw new ResponseStatusException(HttpStatus.NOT_FOUND);
            }

            return new SalesOrderDto(account);
        }

        catch (IllegalArgumentException e) {

            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        catch (Exception e) {

            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    void post(@RequestBody SalesOrderDto salesOrderDto) {

        try {

            service.create(new SalesOrder(salesOrderDto));
        }

        catch (DataIntegrityViolationException e) {

            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        catch (Exception e) {

            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    void put(@PathVariable("id") long id, @RequestBody SalesOrderDto salesOrderDto) {

        try {

            service.update(new SalesOrder(salesOrderDto));
        }

        catch (DataIntegrityViolationException e) {

            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        catch (Exception e) {

            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

